﻿using System;
using System.Collections.Generic;
using UIKit;

namespace MobileAppClass
{
	public partial class DetailViewController : UIViewController
	{

		FileManager file1;
		GameData newentry;
		bool edit;
		int index;


		public DetailViewController(GameData ne,int ind,bool ed) : base("DetailViewController", null)
		{
			file1 = FileManager.getinstance();
			newentry = ne;
			edit = ed;
			index = ind;

		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void ViewDidAppear(bool animated)
		{

			base.ViewDidAppear(animated);

		}

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

			if (edit)
			{


				optxt.Text = newentry.opponent;
				dttm.Date = (Foundation.NSDate)newentry.date;
				pftxt.Text = Convert.ToString(newentry.points);
				patxt.Text = Convert.ToString(newentry.pointsa);

				 
			
			}

		}

		public override void ViewWillDisappear(bool animated)
		{
			bool save = true;

			if ( optxt.Text == "" || pftxt.Text == "" || patxt.Text == ""  )
			{
				UIAlertController ac;
				ac = UIAlertController.Create("Error", "Invalid Input", UIAlertControllerStyle.Alert);
				this.PresentViewController(ac, false, null);
				var cancelButton = UIAlertAction.Create("Try Again", UIAlertActionStyle.Cancel, null);
				ac.AddAction(cancelButton);
				save = false;

				optxt.Text = " ";
				dttm.Date  = (Foundation.NSDate)DateTime.Now;
				pftxt.Text = "2";
				patxt.Text = "2";


			}

			newentry.opponent = optxt.Text;
			newentry.date = ((System.DateTime)dttm.Date).ToLocalTime();
			newentry.points = Convert.ToInt32(pftxt.Text);
			newentry.pointsa = Convert.ToInt32(patxt.Text);

			if (newentry.opponent.ToLower().Contains("bilitski") && newentry.winloss().ToLower() == "loss")
			{
				UIAlertController ac;
				ac = UIAlertController.Create("Error", "Bilitski always loses", UIAlertControllerStyle.Alert);
				this.PresentViewController(ac, false, null);
				var cancelButton = UIAlertAction.Create("I Agree!", UIAlertActionStyle.Cancel, null);
				ac.AddAction(cancelButton);
				save = false;

			}

			if (newentry.points < 0 || newentry.pointsa < 0)
			{
				UIAlertController ac;
				ac = UIAlertController.Create("Error", "Negative Numbers", UIAlertControllerStyle.Alert);
				this.PresentViewController(ac, false, null);
				var cancelButton = UIAlertAction.Create("Try Again", UIAlertActionStyle.Cancel, null);
				ac.AddAction(cancelButton);
				save = false;

			}



			if (save)
			{
				List<GameData> adatalist = new List<GameData>();

				adatalist = file1.Read();

				if (edit)
				{

					adatalist[index] = newentry;

				}
				else
				{
					adatalist.Add(newentry);
				}



				file1.Write(adatalist);
			}
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}

}

