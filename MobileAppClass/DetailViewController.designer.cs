// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MobileAppClass
{
    [Register ("DetailViewController")]
    partial class DetailViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIDatePicker dttm { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField optxt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField patxt { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField pftxt { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (dttm != null) {
                dttm.Dispose ();
                dttm = null;
            }

            if (optxt != null) {
                optxt.Dispose ();
                optxt = null;
            }

            if (patxt != null) {
                patxt.Dispose ();
                patxt = null;
            }

            if (pftxt != null) {
                pftxt.Dispose ();
                pftxt = null;
            }
        }
    }
}