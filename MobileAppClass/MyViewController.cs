﻿using System;
using System.Collections.Generic;
using UIKit;

namespace MobileAppClass
{
	
	
    public partial class MyViewController : UIViewController
    {
		FileManager file;
		private List<GameData> Games = new List<GameData>();
		GameData blankData = new GameData();

        public MyViewController() : base("MyViewController", null)
        {
			
			file = FileManager.getinstance();
		
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.


		}

		public override void ViewWillAppear(bool animated)
		{

	
				
			


			base.ViewWillAppear(animated);
			Games = file.Read();
			tableview1.Source = new TableSource(Games, this);

			if (Games.Count > 0)
			{

				hidden_label.Hidden = true;
				tableview1.Hidden = false;

			}
			else
			{

				hidden_label.Hidden = false;
				tableview1.Hidden = true;
			}



		}

		public override void ViewDidAppear(bool animated)
		{

			base.ViewDidAppear(animated);
			// Perform any additional setup after loading the view, typically from a nib.



			NavigationItem.Title = "Spike Ball Game Info";
			UIBarButtonItem plus = new UIBarButtonItem("+", UIBarButtonItemStyle.Done, newgame);
			NavigationItem.RightBarButtonItem = plus;



		}

		private void newgame(Object sender, EventArgs args)
		{

			var vc = new DetailViewController(blankData, 1, false);
			NavigationController.PushViewController(vc, true);

		
		}

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

	
    }
}

