﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace MobileAppClass
{
	public class FileManager
	{
		public static FileManager instance;
		private string pathFile;
		private List<GameData> data { get; set; }

		JsonSerializerSettings mySettings;
		JsonSerializer mySerializer;

		private FileManager()
		{
			data = new List<GameData>();

			var path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

			pathFile = Path.Combine(path, "spikeballdata.txt");
			Console.WriteLine(pathFile);

			mySettings = new JsonSerializerSettings();
			mySettings.TypeNameHandling = TypeNameHandling.All;
			mySerializer = JsonSerializer.Create(mySettings);



		}

		public static FileManager getinstance()
		{
			if (instance == null)
			{

				return instance = new FileManager();

			}
			else
			{

				return instance;
			
			}


		}

		public List<GameData> Read()
		{
			if (!File.Exists(pathFile))
			{

				return data;
			
			}

			using (var streamReader = new StreamReader(pathFile))
			{


				var myJsonFromFile = streamReader.ReadToEnd();
				data = JsonConvert.DeserializeObject<List<GameData>>(myJsonFromFile);

				if (data != null)
				{
					return data;
				}
				else
				{

					data = new List<GameData>();
					return data;
				
				}

			
			}

		}

		public void Write(List<GameData> data)
		{

			data.Sort((x, y) => DateTime.Compare(x.date, y.date));

			var myJson = JsonConvert.SerializeObject(data);

			using (var streamwriter = new StreamWriter(pathFile, false))
			{
				streamwriter.WriteLine(myJson);
			}

		}
		//public void Write(GameData data)
		//{

		//	var myJson = JsonConvert.SerializeObject(data);

		//	using (var streamwriter = new StreamWriter(pathFile,true))
		//	{
		//		streamwriter.WriteLine(myJson);
		//	}

		//}

	}
}
