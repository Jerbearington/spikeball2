﻿using System;
using System.IO;
namespace MobileAppClass
{
	public class GameData
	{
		public string opponent { get; set;}
		public DateTime date { get; set;}
		public int points { get; set;}
		public int pointsa { get; set;}

		public GameData()
		{ 
		
		}

		public GameData(string op, DateTime dt, int ptf, int pta)
		{
			opponent = op;
			date = dt;
			points = ptf;
			pointsa = pta;



		}

		public string winloss()
		{

			if (points > pointsa)
			{

				return "Win";

			}
			else if (points == pointsa)
			{

				return "Tie";
			
			}
			else
			{

				return  "Loss";
			
			}
		
		}

		public override string ToString()
		{
			return string.Format("[GameData: opponent={0}, date={1}, points={2}, pointsa={3}]", opponent, date, points, pointsa);
		}

	}
}
