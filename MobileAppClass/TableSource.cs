﻿using System;
using System.Collections.Generic;
using UIKit;

namespace MobileAppClass
{



	public class TableSource : UITableViewSource
	{

		private List<GameData> data;
		MyViewController vc;


		public TableSource(List<GameData> datain, MyViewController mvc)
		{

			data = datain;

			vc = mvc;
			
		}


		public override nint NumberOfSections(UITableView tableView)
		{

			return 1;
		
		}

		public override UITableViewCell GetCell(UITableView tableView, Foundation.NSIndexPath indexPath)
		{

			UITableViewCell cell;

			cell = tableView.DequeueReusableCell("spikeBall");

			if (cell == null)
			{

				cell = new UITableViewCell(UITableViewCellStyle.Default, "spikeball");

				cell = new UITableViewCell(UITableViewCellStyle.Subtitle, "spikeball");

			}


			cell.TextLabel.Text = data[indexPath.Row].opponent;

			cell.DetailTextLabel.Text = data[indexPath.Row].winloss() + " | " + data[indexPath.Row].date;
			cell.Accessory = UITableViewCellAccessory.DisclosureIndicator;

			return cell;

		}

		public override void RowSelected(UITableView tableView, Foundation.NSIndexPath indexPath)
		{

			DetailViewController dvc = new DetailViewController(data[indexPath.Row],indexPath.Row,true);
			vc.NavigationController.PushViewController(dvc, true);


		}

		public override nint RowsInSection(UITableView tableview, nint section)
		{
			if (data != null)
				return data.Count;
			return 1;
		}


	}
}
